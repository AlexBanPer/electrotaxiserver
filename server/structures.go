package main

import "time"

//ClientsList
var ClientList = []Client{
	{
		ClientID:       "testingID",
		ClientIP:       "127.0.0.1",
		ClientPosition: GPSLocation{
			XPosition: "-33.0214",
			ZPosition: "-71.55",
		},
	},
	{
		ClientID:       "AlexABCDE",
		ClientIP:       "127.0.0.2",
		ClientPosition: GPSLocation{
			XPosition: "-33.0214",
			ZPosition: "-71.565",
		},
	},
	{
		ClientID:       "ID2",
		ClientIP:       "127.0.0.2",
		ClientPosition: GPSLocation{
			XPosition: "-33.0214",
			ZPosition: "-71.565",
		},
	},
	{
		ClientID:       "ID3",
		ClientIP:       "127.0.0.2",
		ClientPosition: GPSLocation{
			XPosition: "-33.0214",
			ZPosition: "-71.565",
		},
	},
}
var VehicleList = []Vehicle{
	{
		VehicleID: "TestV1",
		Location:  GPSLocation{
			XPosition: "-33.02457",
			ZPosition: "-71.55183",
		},
		Status:    0,
		rfID:      "TestVehicleRfID",
	},
	{
		VehicleID: "TestV2",
		Location:  GPSLocation{
			XPosition: "-33.02457",
			ZPosition: "-71.553",
		},
		Status:    0,
		rfID:      "TestVehicleRfID2",
	},
	{
		VehicleID: "TestV2Away",
		Location:  GPSLocation{
			XPosition: "-33.02457",
			ZPosition: "-70.553",
		},
		Status:    0,
		rfID:      "TestVehicleRfID22",
	},
	{
		VehicleID: "TestV2Low",
		Location:  GPSLocation{
			XPosition: "-33.02457",
			ZPosition: "-71.563",
		},
		Status:    1,
		rfID:      "TestVehicleRfID11",
	},
}

var InRouteList []MatchClientVehicle
var UserInRoute []Client

type GPSLocation struct{
	XPosition string
	ZPosition string
}

type Client struct{
	ClientID string
	ClientIP string
	ClientPosition GPSLocation
	ClientDestination GPSLocation
}

type MatchClientVehicle struct {
	TravelID string
	Client Client
	Vehicle Vehicle
	StartLocation GPSLocation
	EndLocation GPSLocation
	Markers []MarkersMap
	Status int
}

type MarkersMap struct {
	Location GPSLocation
	Stamp time.Time
	Stats Stats
}

type Stats struct {
	Speed int
	Odometer int
	Price int
}


type Vehicle struct{
	VehicleID string
	Location GPSLocation
	Status int
	rfID string
}



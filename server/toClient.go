package main

import (
	"fmt"
	"strings"
)

func RemoveFromArray(toRemove []string, index int) []string{
	a := toRemove
	i := index

	// Remove the element at index i from a.
	copy(a[i:], a[i+1:]) // Shift a[i+1:] left one index.
	a[len(a)-1] = ""     // Erase last element (write zero value).
	a = a[:len(a)-1]     //

	return a
}

func sendToClient(receive string) {
	data := strings.Split(receive, ",") //I am a robot **

	for index, element := range data {
		data[index] = strings.Replace(element, "\r\n\r\n", "", -1)
	}



	//
	if len(data) < 3 {
		fmt.Printf("\nData corrupted\n D: %v \n", data)
		return
	}
	data = RemoveFromArray(data, 3)


	fmt.Printf("\nDATA: %v\n", data)

	newClient := Client{
		ClientID: data[0],
		ClientIP: data[1],
		ClientPosition: GPSLocation{
			XPosition: data[2],
			ZPosition: data[3],
		},
	}
	fmt.Printf("\n STRUCT: %v \n", newClient)
	//ClientList = append(ClientList, newClient)
}

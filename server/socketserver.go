package main

import (
	"flag"
	"fmt"
	"github.com/googollee/go-socket.io"
	"log"
	"net/http"
	"time"
	_ "time"
	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:8080", "http service address")
var upgrader = websocket.Upgrader{}

type customServer struct {
	Server *socketio.Server
}

var SocketPort = fmt.Sprintf(":%v", ServerEventPort)

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		allowHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization"

		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://200.120.1.23")
		w.Header().Set("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		w.Header().Set("Access-Control-Allow-Credentials", "false")
		w.Header().Set("Access-Control-Allow-Headers", allowHeaders)

		next.ServeHTTP(w, r)
	})
}

func StartEventServer(){
	fmt.Printf("Starting Event Server\n")
	time.Sleep(time.Second * 2)

	server, err := socketio.NewServer(nil)
	if err != nil {
		println(err)
		log.Printf("Retrying to connect...")
		time.Sleep(time.Second * 5)
		StartEventServer()
		return
	}


	server.OnConnect("/", func(s socketio.Conn) error {
		s.Emit("connected", "")
		s.SetContext("")
		Log(fmt.Sprintf("New connection: %v IP: %v", s.ID(), s.RemoteAddr()))

		return nil
	})

	server.OnEvent("/", "getClients", func(s socketio.Conn) {
		Log(fmt.Sprintf("Client Request: %v \nGet clients: %v", s.ID(), ClientList))
		s.Emit("getClients", ClientList)
	})

	server.OnEvent("/", "getVehicles", func(s socketio.Conn, msg string) {
		Log(fmt.Sprintf("Client Request: %v \nGet VehicleList: %v", s.ID(), VehicleList))
		s.Emit("getVehicles", VehicleList)
	})

	server.OnEvent("/", "getClient", func(s socketio.Conn, ClientID string) {
		fmt.Println("Search Client:", interface{}(SearchClient(ClientID)))
		s.Emit("getClient", SearchClient(ClientID))
	})

	/* CLIENTS */

	server.OnEvent("/", "newClient", func(s socketio.Conn, client Client) {
		fmt.Println("New Client:", client)
		s.Emit("GetAvailableVehicles", VehicleList)
	})

	server.OnEvent("/", "newTravel", func(s socketio.Conn, clientID string, destination GPSLocation) {
		fmt.Println("New Travel destination:", destination)
		s.Emit("TravelValue", 5224) //TODO Adjust value from real
	})

	server.OnEvent("/", "newTravelConfirm", func(s socketio.Conn, clientID string, destination GPSLocation) {

		if !IsClientInRoute(clientID) {
			Log(fmt.Sprintf("New Travel:: %v destination confirmed: %v", clientID, destination))

			AddClientToWaitList(clientID, destination)
			time.Sleep(time.Millisecond * 100)
			client := SearchClient(clientID)
			travel := CreateTravel(client, destination)
			Log(fmt.Sprintf("New travel created %v by %v", travel, client))
			time.Sleep(time.Millisecond * 100)
			s.Emit("WaitingVehicle", travel)
		} else {
			s.Emit("AlreadyOnRoute")
		}

	})

	server.OnEvent("/", "getMyTravel", func(s socketio.Conn, clientID string) {
		Log(fmt.Sprintf("Get Travel:: %v ", clientID))

		time.Sleep(time.Millisecond * 100)
		client := SearchClient(clientID)
		travel := SearchTravel(client)
		Log(fmt.Sprintf("Getting travel %v by %v", travel, client))
		time.Sleep(time.Millisecond * 100)
		s.Emit("getMyTravel", travel)
	})

	server.OnEvent("/", "cancelTravel", func(s socketio.Conn, clientID string, travelID string) {
		Log(fmt.Sprintf("Canceling Travel:: %v for client: %v", travelID, clientID))

		//TODO Cancel travel sending notification to both services

		s.Emit("CanceledTravel")
	})


	/* VEHICLES */
	server.OnEvent("/", "newService", func(s socketio.Conn, vehicle Vehicle) {
		fmt.Println("New available vehicle:", vehicle)
		s.Emit("UpdateToWait")
	})


	server.OnEvent("/", "bye", func(s socketio.Conn) string {
		last := s.Context().(string)
		s.Emit("bye", last)
		s.Close()
		return last
	})

	server.OnError("/", func(s socketio.Conn, e error) {
		fmt.Println("meet error:", e)
	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		fmt.Println("closed", reason)
	})

	go server.Serve()
	defer server.Close()

	mux := http.NewServeMux()
	mux.Handle("/socket.io/", server)
	mux.HandleFunc("/", test)

	time.Sleep(time.Second * 2)
	ClearConsole()

	fmt.Printf("Starting Socket Server on port %v\n", SocketPort)
	if http.ListenAndServe(SocketPort, nil) != nil{
		log.Printf("Can't create socket server....\nTrying again...")
		time.Sleep(time.Second * 5)
		StartEventServer()
	}
}

func test(w http.ResponseWriter, request *http.Request) {
	setupResponse(&w)
}

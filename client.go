package main

import (
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

const (
	message       = "Hello,i,am,a,robot"
	StopCharacter = "\r\n\r\n"
)

func SocketClient(ip string, port int) {


	for {
		time.Sleep(time.Second * 5)
		addr := strings.Join([]string{ip, strconv.Itoa(port)}, ":")
		conn, err := net.Dial("tcp", addr)

		if err != nil {
			fmt.Printf("Err %v", err)
			log.Printf("Reintentando conexion...")
			time.Sleep(time.Second * 2)
			SocketClient(ip, port)
			break
		}

		defer conn.Close()

		conn.Write([]byte(message))
		conn.Write([]byte(StopCharacter))
		log.Printf("Send: %s", message)

		buff := make([]byte, 1024)
		n, _ := conn.Read(buff)
		log.Printf("Receive: %s", buff[:n])
	}
}

func main() {

	var (
		ip   = "127.0.0.1"
		port = 3333
	)

	SocketClient(ip, port)

}